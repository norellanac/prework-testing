import { add, subtract } from "../calculator";
import { test, expect } from "@jest/globals";

test("adds two numbers correctly", () => {
  expect(add(2, 3)).toBe(5);
});

test("subtracts two numbers correctly", () => {
  expect(subtract(5, 3)).toBe(2);
});

// Add more test cases for the other calculator functions
